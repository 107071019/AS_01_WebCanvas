var draw; //是否在繪圖狀態
var m, c; //繪圖物件
var r,g,b,penSize;

var canvas;
var ctx;
var gradient;
var canvas2;
var ctx2;
var gradient2;
var x;
var x2;
var rgb=[255,0,0];
var color="black";

var draw_rec;
var draw_fillrec;
var draw_cir;
var draw_fillcir;
var draw_tri;
var draw_filltri;

var text_input=false;
var text_done=false;
var img_input=false;
var rec_input=false;
var fillrec_input=false;
var cir_input=false;
var fillcir_input=false;
var tri_input=false;
var filltri_input=false;


var last_x,last_y;
var rec_startx,rec_starty;
var fillrec_startx,filrec_starty;
var cir_startx,cir_starty;
var fillcir_startx,fillcir_starty;
var tri_startx, tri_starty;
var filltri_startx, filltri_starty;

var cPushArray = new Array();
var cStep = -1;

var canvas_before_shape=new Image();


var image= new Image();
var reader = new FileReader;

reader.onloadend = function () {
    //document.images[0].src = this.result;
    image.src=this.result;
}    


function cPush() {
    cStep++;
    if (cStep < cPushArray.length) { cPushArray.length = cStep; }
    cPushArray.push(document.getElementById("myCanvas").toDataURL());
}    
function undo() {
    if (cStep > 0) {
        cStep--;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { 
            c.clearRect(0,0,700,500);
            c.drawImage(canvasPic, 0,0, 700,500,0,0,700,500); 
        }
    }
}   
function redo() {
    if (cStep < cPushArray.length-1) {
        cStep++;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        canvasPic.onload = function () { 
            c.clearRect(0,0,700,500);
            c.drawImage(canvasPic, 0, 0,700,500,0,0,700,500); }
    }
}      


function show_textBox(){
    $("#myCanvas").click(function (e) {
        if (text_input) {
            console.log("input...");
            let t = $('<input id="box" type="text">')
                .css({
                    left: e.pageX + 'px',
                    top: e.pageY + 'px'
                })
                .appendTo(document.body);
            //last_x = e.pageX;
           // last_y = e.pageY;
            text_done=true;
            text_input=false;
        }
    });
    
}
function show_text(){
    
    document.body.addEventListener('click', function () {
        if (text_done){
            //console.log("click");
            let txtBox = document.getElementById("box");
            let word = document.getElementById("box").value;
            if (word != "" && text_done) {
                var fontSize=document.getElementById("fontSize").value;
                var fontStyle=document.getElementById("fontStyle").value;
                c.font =fontSize+"px "+fontStyle;
                console.log(c.font);
                c.fillText(word, last_x, last_y );
                txtBox.remove();
                cPush();
                text_done=false;
                text_input=true;
        }
        }
        
    });
}


function init() {
    
    m = document.getElementById("myCanvas"); //取得畫布物件參考
    c = m.getContext("2d"); //建立2d繪圖物件
    cPush();

    canvas = document.getElementById("demoCanvas");  
    if (canvas.getContext)   
    {  
        ctx = canvas.getContext("2d");         
        gradient = ctx.createLinearGradient(0, 0, 255, 0);
        gradient.addColorStop(0, "rgb(255,0,0)");
        gradient.addColorStop(1 / 6, "rgb(255,255,0)");
        gradient.addColorStop(2 / 6, "rgb(0,255,0)");
        gradient.addColorStop(3 / 6, "rgb(0,255,255)");
        gradient.addColorStop(4 / 6, "rgb(0,0,255)");
        gradient.addColorStop(5 / 6, "rgb(255,0,255)");
        gradient.addColorStop(1, "rgb(255,0,0)");
        ctx.fillStyle = gradient;
        ctx.fillRect(0, 0, 255, 40);
    }

    canvas2 = document.getElementById("demoCanvas2");  
    if (canvas2.getContext)   
    {  
        ctx2 = canvas2.getContext("2d");         
        gradient2 = ctx2.createLinearGradient(0, 0, 255, 0);
        gradient2.addColorStop(0, "black");
        
        gradient2.addColorStop(3 / 6, "red");
        
        gradient2.addColorStop(1, "white");
        ctx2.fillStyle = gradient2;
        ctx2.fillRect(0, 0, 255, 40);
    }

    
}
function md() {
    console.log(c.strokeStyle);
    c.moveTo(event.offsetX, event.offsetY); //起點
    if (text_input){
        c.globalCompositeOperation="source-over";
        console.log("record  text input x,y offset");
        last_x=event.offsetX;
        last_y=event.offsetY;
        show_textBox();
    }
    else if (text_done){
        console.log("txt done");
        show_text();
    }
    else if(img_input){
        c.globalCompositeOperation="source-over";
        c.drawImage(image,event.offsetX,event.offsetY);
    }
    else if(rec_input){
        console.log("rec x,y offset");
        rec_startx=event.offsetX;
        rec_starty=event.offsetY;
        draw_rec=true;
        //c.beginPath();
    }
    else if(fillrec_input){
        console.log("fillrec x,y offset");
        fillrec_startx=event.offsetX;
        fillrec_starty=event.offsetY;
        draw_fillrec=true;
        //c.beginPath();
    }
    else if(cir_input){
        console.log("cir x,y offset");
        cir_startx=event.offsetX;
        cir_starty=event.offsetY;
        draw_cir=true;
        //c.beginPath();
    }
    else if(fillcir_input){
        console.log("fillcir x,y offset");
        fillcir_startx=event.offsetX;
        fillcir_starty=event.offsetY;
        draw_fillcir=true;
        //c.beginPath();
    }
    else if(tri_input){
        console.log("tri x,y offset");
        tri_startx=event.offsetX;
        tri_starty=event.offsetY;
        draw_tri=true;
    }
    else if(filltri_input){
        console.log("filltri x,y offset");
        filltri_startx=event.offsetX;
        filltri_starty=event.offsetY;
        draw_filltri=true;
    }
    else{
        draw = true; //進入繪圖模式
        c.beginPath(); //本次繪圖筆畫開始
    }
    
}
function mv() {
    if (draw) {
        c.lineTo(event.offsetX, event.offsetY); //下一點
        c.stroke(); //繪圖
    }else if(draw_rec){
        console.log("size of rec change");
        
        
        c.beginPath();
        c.clearRect(0,0,700,500);
        c.drawImage(canvas_before_shape, 0,0, 700,500,0,0,700,500); 
        c.rect(rec_startx,rec_starty,event.offsetX-rec_startx,event.offsetY-rec_starty);
        c.stroke();
    }else if(draw_fillrec){
        console.log("size of fillrec change");
        
        
        c.beginPath();
        c.clearRect(0,0,700,500);
        c.drawImage(canvas_before_shape, 0,0, 700,500,0,0,700,500); 
        c.fillRect(fillrec_startx,fillrec_starty,event.offsetX-fillrec_startx,event.offsetY-fillrec_starty);
        c.stroke();
    }else if(draw_cir){
        console.log("size of cir change");

        var tmp=(event.offsetX-cir_startx)*(event.offsetX-cir_startx)+(event.offsetY-cir_starty)*(event.offsetY-cir_starty);
        var radius=Math.sqrt(tmp)/2;
        
        c.beginPath();
        c.clearRect(0,0,700,500);
        c.drawImage(canvas_before_shape, 0,0, 700,500,0,0,700,500); 
        c.arc((event.offsetX+cir_startx)/2,(event.offsetY+cir_starty)/2,radius,0,2*Math.PI);
        c.stroke();
    }else if(draw_fillcir){
        console.log("size of fillcir change");

        var tmp=(event.offsetX-fillcir_startx)*(event.offsetX-fillcir_startx)+(event.offsetY-fillcir_starty)*(event.offsetY-fillcir_starty);
        var radius=Math.sqrt(tmp)/2;
        
        c.beginPath();
        c.clearRect(0,0,700,500);
        c.drawImage(canvas_before_shape, 0,0, 700,500,0,0,700,500); 
        c.arc((event.offsetX+fillcir_startx)/2,(event.offsetY+fillcir_starty)/2,radius,0,2*Math.PI);
        c.fill();
        c.stroke();
    }else if(draw_tri){
        console.log("size of tri change");

        c.beginPath();
        c.clearRect(0,0,700,500);
        c.drawImage(canvas_before_shape, 0,0, 700,500,0,0,700,500); 
        c.moveTo((tri_startx+event.offsetX)/2, tri_starty);
        c.lineTo(tri_startx, event.offsetY);
        c.lineTo(event.offsetX, event.offsetY);
        c.closePath();
        c.stroke();
    }else if(draw_filltri){
        console.log("size of filltri change");

        c.beginPath();
        c.clearRect(0,0,700,500);
        c.drawImage(canvas_before_shape, 0,0, 700,500,0,0,700,500); 
        c.moveTo((filltri_startx+event.offsetX)/2, filltri_starty);
        c.lineTo(filltri_startx, event.offsetY);
        c.lineTo(event.offsetX, event.offsetY);
        c.fill();
        c.closePath();
        c.stroke();
    }
    
}
function mu() {
    if(draw_rec){
        draw_rec=false;
        use_rec();
    }else if(draw_fillrec){
        draw_fillrec=false;
        use_fill_rec();
    }
    else if(draw_cir){
        draw_cir=false;
        use_cir();
    }
    else if(draw_fillcir){
        draw_fillcir=false;
        use_fill_cir();
    }
    else if(draw_tri){
        draw_tri=false;
        use_tri();
    }
    else if(draw_filltri){
        draw_filltri=false;
        use_fill_tri();
    }
    
    draw = false; //離開繪圖模式
    c.closePath(); //繪圖筆畫結束
    if(!text_input && !text_done)  cPush();
    
}




function mouse_down(){
    x=event.offsetX;
    x=x*6;
    
    if (x<=255){
        rgb[0]=255;
        rgb[1]=x;
        rgb[2]=0;
    }else if (x<= (255*2) ){
        var tmp=x-255;
        rgb[0]=255-tmp;
        rgb[1]=255;
        rgb[2]=0;
        
    }else if (x<= (255*3) ){
        var tmp=x-(255*2);
        rgb[0]=0;
        rgb[1]=255;
        rgb[2]=tmp;
        
    }else if (x<= (255*4) ){
        var tmp=x-(255*3);
        rgb[0]=0;
        rgb[1]=255-tmp;
        rgb[2]=255;
        
    }else if (x<= (255*5) ){
        var tmp=x-(255*4);
        rgb[0]=tmp;
        rgb[1]=0;
        rgb[2]=255;
        
    }else if (x<= (255*6) ){
        var tmp=x-(255*5);
        rgb[0]=255;
        rgb[1]=0;
        rgb[2]=255-tmp;
        
    }
    color="rgb("+rgb[0]+","+rgb[1]+","+rgb[2]+")";
    c.strokeStyle=color;
    c.fillStyle=color;
    c.shadowColor=color;
    document.getElementById("color_demo").style.fill=color;


    gradient2 = ctx2.createLinearGradient(0, 0, 255, 0);
    gradient2.addColorStop(0, "black");
        
    gradient2.addColorStop(3 / 6, color);
        
    gradient2.addColorStop(1, "white");
    ctx2.fillStyle = gradient2;
    ctx2.fillRect(0, 0, 255, 40);

}
function mouse_down2(){
    x2=event.offsetX;
    x2=x2*2;
    
    var r,g,b;
    if (x2<=255){
        var tmp=x2;
        var tmp_r=rgb[0]/255;
        r=tmp_r*tmp;

        var tmp_g=rgb[1]/255;
        g=tmp_g*tmp;

        var tmp_b=rgb[2]/255;
        b=tmp_b*tmp;
        console.log("rgb("+r+","+g+","+b+")");
    }else{
        var tmp=x2-255;
        var tmp_r=(255-rgb[0])/255;
        r=rgb[0]+tmp_r*tmp;

        var tmp_g=(255-rgb[1])/255;
        g=rgb[1]+tmp_g*tmp;

        var tmp_b=(255-rgb[2])/255;
        b=rgb[2]+tmp_b*tmp;
        console.log("rgb("+r+","+g+","+b+")");
    }

    color="rgb("+r+","+g+","+b+")";
    c.strokeStyle=color;
    c.fillStyle=color;
    c.shadowColor=color;
    document.getElementById("color_demo").style.fill=color;
}


function btn_rst(){
    location.reload();
}
function btn_prev(){
    c.globalCompositeOperation="source-over";
    undo();
    console.log("pre");
}
function btn_next(){
    c.globalCompositeOperation="source-over";
    redo();
    console.log("next");
}


function save_pic(){
    var pic_url=m.toDataURL();
    document.getElementById("save").href = pic_url;
}




function set_shadow(){
    console.log(c.shadowColor);
    c.shadowBlur=document.getElementById("shadow").value;
    c.shadowColor=color;
}
function set_penSize(){
    penSize=document.getElementById("psize").value;
    c.lineWidth=penSize;
}


function use_pen(){
    tri_input=false;
    filltri_input=false;
    cir_input=false;
    fillcir_input=false;
    fillrec_input=false;
    rec_input=false;
    img_input=false;
    text_input=false;
    text_done=false;
    console.log("pen");
    c.globalCompositeOperation="source-over";
    document.getElementById("myCanvas").style.cursor="url('oth505.cur'),auto";
}
function use_eraser(){
    filltri_input=false;
    tri_input=false;
    cir_input=false;
    fillcir_input=false;
    fillrec_input=false;
    rec_input=false;
    img_input=false;
    text_input=false;
    text_done=false;
    console.log("eraser");
    c.globalCompositeOperation="destination-out";
    document.getElementById("myCanvas").style.cursor="url('Eraser.cur'),auto";
}
function use_image(){
    filltri_input=false;
    tri_input=false;
    cir_input=false;
    fillcir_input=false;
    fillrec_input=false;
    rec_input=false;
    text_input=false;
    text_done=false;
    console.log("img");
    c.globalCompositeOperation="source-over";
    document.getElementById("myCanvas").style.cursor="url('Precision Select.cur'),auto";

    img_input=true;
    
}
function uploading(){
    reader.readAsDataURL(document.getElementById("input").files[0]);
}
function use_rec(){
    filltri_input=false;
    tri_input=false;
    cir_input=false;
    fillcir_input=false;
    fillrec_input=false;
    rec_input=true;
    img_input=false;
    text_input=false;
    text_done=false;
    console.log("rec");
    c.globalCompositeOperation="source-over";
    document.getElementById("myCanvas").style.cursor="url('prec.cur'),auto";
    
    canvas_before_shape.src = document.getElementById("myCanvas").toDataURL();
}
function use_cir(){
    filltri_input=false;
    tri_input=false;
    cir_input=true;
    fillcir_input=false;
    fillrec_input=false;
    rec_input=false;
    img_input=false;
    text_input=false;
    text_done=false;
    console.log("rec");
    c.globalCompositeOperation="source-over";
    document.getElementById("myCanvas").style.cursor="url('prec.cur'),auto";
    
    canvas_before_shape.src = document.getElementById("myCanvas").toDataURL();
}
function use_tri(){
    filltri_input=false;
    tri_input=true;
    cir_input=false;
    fillcir_input=false;
    fillrec_input=false;
    rec_input=false;
    img_input=false;
    text_input=false;
    text_done=false;
    console.log("tri");
    c.globalCompositeOperation="source-over";
    document.getElementById("myCanvas").style.cursor="url('prec.cur'),auto";
    
    canvas_before_shape.src = document.getElementById("myCanvas").toDataURL();
}
function use_fill_rec(){
    filltri_input=false;
    tri_input=false;
    cir_input=false;
    fillcir_input=false;
    fillrec_input=true;
    rec_input=false;
    img_input=false;
    text_input=false;
    text_done=false;
    console.log("fillrec");
    c.globalCompositeOperation="source-over";
    document.getElementById("myCanvas").style.cursor="url('prec.cur'),auto";
    
    canvas_before_shape.src = document.getElementById("myCanvas").toDataURL();
}
function use_fill_cir(){
    filltri_input=false;
    tri_input=false;
    cir_input=false;
    fillcir_input=true;
    fillrec_input=false;
    rec_input=false;
    img_input=false;
    text_input=false;
    text_done=false;
    console.log("fillcir");
    c.globalCompositeOperation="source-over";
    document.getElementById("myCanvas").style.cursor="url('prec.cur'),auto";

    canvas_before_shape.src = document.getElementById("myCanvas").toDataURL();
    
}
function use_fill_tri(){
    filltri_input=true;
    tri_input=false;
    cir_input=false;
    fillcir_input=false;
    fillrec_input=false;
    rec_input=false;
    img_input=false;
    text_input=false;
    text_done=false;
    console.log("filltri");
    c.globalCompositeOperation="source-over";
    document.getElementById("myCanvas").style.cursor="url('prec.cur'),auto";
    
    canvas_before_shape.src = document.getElementById("myCanvas").toDataURL();
}






function typing(){
    filltri_input=false;
    tri_input=false;
    cir_input=false;
    fillcir_input=false;
    fillrec_input=false;
    rec_input=false;
    image_input=false;
    text_input=true;
    text_done=false;
    document.getElementById("myCanvas").style.cursor="url('Text Select.cur'),auto";
}
