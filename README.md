# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | Y         |


---

### How to use 

>![](https://i.imgur.com/r1AUaob.png)
>下載圖片

>![](https://i.imgur.com/ICQ9rUa.png)
>點擊圖片調色

>![](https://i.imgur.com/mxBZ5vx.png)
>拉動slider控制showdowBlur的程度

>![](https://i.imgur.com/hBOg71N.png)
>拉動slider控制筆刷大小

>![](https://i.imgur.com/NiIp5UZ.png)
>點擊TEXT按鈕進入文字編輯，滑鼠點擊畫布，會出現文字框供輸入。輸入完畢，點擊畫布，結束輸入。若想再次輸入，再點擊畫布，會再次出現文字框。可更改字體大小和字型。

>![](https://i.imgur.com/mKGKaUh.png)
>點擊進入畫筆模式

>![](https://i.imgur.com/pHHDY19.png)
>點擊進入橡皮擦模式

>![](https://i.imgur.com/10lROIq.png)
>選擇上傳圖片後，於畫布上想要的位置點下滑鼠，圖片就會出現在該位置，再次點擊圖片就會再出現，像蓋印章一樣。

>![](https://i.imgur.com/JaA4kHZ.png)
>點擊進入畫空心四邊形模式

>![](https://i.imgur.com/17NN47g.png)
>點擊進入畫空心圓形模式

>![](https://i.imgur.com/ifF7ofN.png)
>點擊進入畫空心三角形模式

>![](https://i.imgur.com/LCSPXE0.png)
>點擊進入畫實心四邊形模式

>![](https://i.imgur.com/8OjMFch.png)
>點擊進入畫實心圓形模式

>![](https://i.imgur.com/m6ApkQV.png)
>點擊進入畫實心三角形模式

>![](https://i.imgur.com/7Fd0FZx.png)
>undo

>![](https://i.imgur.com/VjnqEcr.png)
>redo

>![](https://i.imgur.com/bkd5ecU.png)
>reset


### Function description

1. brush and eraser : 分別用"source-over"和"destination-out"控制。
```javascript=true
c.globalCompositeOperation="source-over";
c.globalCompositeOperation="destination-out";
```
2. Color selector : 用兩個畫布展示用兩個畫布展示所有可以選擇的顏色，一個是橫向漸變(由左而右rgb輪流增減)、一個是縱向漸變(向左漸漸歸零，向右漸漸加到255)。透過比例換算顏色，並顯示在上方的長格中。
3. Simple menu (brush size):slider的value(用DOM取得)就是調整後的筆刷大小。
4. Text input : 用input中的type=text來展示( .appendTo(document.body) )文字框，輸入之後再移除( .remove() )。
5. Cursor icon : style中的cursor改變icon。
6. Refresh button : 重新載入頁面
```javascript=true
location.reload();
```
7. Different brush shapes : 正方形(起點是mouse down時的位置，mouse up 時的位置減起點就是長寬)；圓形(起點減終點除以2就是圓心)；三角形(三頂點分別是(起點x,終點y)、(起點x,起終y相加/2)、(終點x,終點y))。

8. Un/Re-do button : 用一個陣列記住每次畫的畫面，並用cStep這個變數紀錄目前所在的畫面。要undo、redo的時候，就重新畫一次畫面。
9. Image tool : 用filereader來讀取( .readAsDataURL() )檔案。
10. Download : 用.toDataURL()取得畫布的url，然後將 <a>tag的href射程該url。
11. Bonus : 用slider的方式調整shadowBlur。
```javascript=1
c.shadowBlur=document.getElementById("shadow").value;
```
    
### Gitlab page link

    your web page URL, which should be "https://107071019.gitlab.io/AS_01_WebCanvas"

### Others (Optional)

    Anythinh you want to say to TAs.

<style>
table th{
    width: 100%;
}
</style>